# LEGO® Hero Factory

- HOME
- **PRODOTTI**
- GIOCHI
- VIDEO
- DOWNLOAD

## PRODOTTI

- EROI
  - [44015 EVO WALKER](/it-IT/themes/Hero-Factory/products/44015.md)
  - [44017 STORMER FREEZE MACHINE](/it-IT/themes/Hero-Factory/products/44017.md)
  - [44018 FURNO JET MACHINE](/it-IT/themes/Hero-Factory/products/44018.md)
  - [44019 ROCKA STEALTH MACHINE](/it-IT/themes/Hero-Factory/products/44019.md)
  - [44022 EVO XL MACHINE](/it-IT/themes/Hero-Factory/products/44022.md)
- BESTIE
  - [44016 JAW BEAST VS. STORMER](/it-IT/themes/Hero-Factory/products/44016.md)
  - [44020 FLYER BEAST VS. BREEZ](/it-IT/themes/Hero-Factory/products/44020.md)
  - [44021 SPLITTER BEAST VS. FURNO &amp; EVO](/it-IT/themes/Hero-Factory/products/44021.md)

### 44021 SPLITTER BEAST VS. FURNO &amp; EVO

![](https://www.lego.com/cdn/product-assets/product.img.pri/44021_prod.jpg)

Attenzione, eroi! SPLITTER Beast è una titanica bestia a due teste che sorveglia il più grande crepaccio di Antropolis City dove si nascondono le creature malvagie. EVO e FURNO, vi abbiamo dotato di un lazo laser. Distraete questo bruto con molti occhi usando la spada e la pistola di fuoco, mentre legate le sue gambe per abbatterlo. Ma attenzione, può improvvisamente dividersi in due e causare un doppio pericolo per voi e per i civili! Include i mini robot EVO e FURNO con armi e accessori.

&copy;2014 The LEGO Group. All rights reserved.